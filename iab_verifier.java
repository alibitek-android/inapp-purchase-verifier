
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;

public class verify {

    public static String KEY_FACTORY_ALGORITHM = "RSA";
    public static String SIGNATURE_ALGORITHM = "SHA1withRSA";

    public static String encodedPublicKey = "xxx";
    public static String signedData = "{\"nonce\":-1582580775222238437,\"orders\":[{\"notificationId\":\"android.test.purchased\",\"orderId\":\"transactionId.android.test.purchased\",\"packageName\":\"com.example.testpay\",\"productId\":\"android.test.purchased\",\"purchaseTime\":1340632834758,\"purchaseState\":0}]}";
    public static String signature = "xxx";

    public static void main(String[] args) {
        try {
            byte[] decodedKey = Base64.decode(encodedPublicKey);
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_FACTORY_ALGORITHM);
            PublicKey publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(decodedKey));

            Signature sig = Signature.getInstance(SIGNATURE_ALGORITHM);
            sig.initVerify(publicKey);
            sig.update(signedData.getBytes());
            System.out.println(sig.verify(Base64.decode(signature)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
 
