#!/usr/bin/python

from Crypto.Hash import SHA
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from base64 import b64decode

def chunks(s, n):
    for start in range(0, len(s), n):
        yield s[start:start+n]

def pem_format(key):
    return '\n'.join([
        '-----BEGIN PUBLIC KEY-----',
        '\n'.join(chunks(key, 64)),
        '-----END PUBLIC KEY-----'
    ])

def validate(publicKey, signedData, signature):
    key = RSA.importKey(pem_format(publicKey))
    verifier = PKCS1_v1_5.new(key)
    data = SHA.new(signedData)
    sig = b64decode(signature)
    return verifier.verify(data, sig)

if __name__ == "__main__":
    publicKey = 'xxx'
    signedData = b'{"nonce":-1582580775222238437,"orders":[{"notificationId":"android.test.purchased","orderId":"transactionId.android.test.purchased","packageName":"com.example.testpay","productId":"android.test.purchased","purchaseTime":1340632834758,"purchaseState":0}]}'
    signature = b'xxx'

    print(validate(publicKey, signedData, signature))
