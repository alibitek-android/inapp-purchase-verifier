from bottle import Bottle, run, template, request, response

app = Bottle()

@app.post('/google')
def google():
    remote_address = request.remote_addr
    js = request.json
    signature = js['signature']
    signedData = js['signedData']
    return "Remote Address: %s\nSignature: %s\nSigned Data: %s\n" % (remote_address, signature, signedData)

@app.post('/apple')
def apple():
    receiptData = request.forms.receiptData

    return "Receipt Data: %s" % receiptData

@app.post('/paypal')
def worldpay():
    redirectMessage = request.forms.redirectMessage
    macSecret = request.forms.mac
    return "Redirect Message: %s\nMAC Secret %s\n" % (redirectMessage, macSecret)


if __name__ == '__main__':
  run(app, host='0.0.0.0', port=8080, debug=True, reloader=True)
